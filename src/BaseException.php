<?php

namespace Glance\ErrorMiddleware;

use Exception;

/**
 * Base exception
 *
 * @author Mario <mario.simao@cern.ch>
 */
class BaseException extends Exception
{
    /**
     * HTTP Status Code
     *
     * @var int
     */
    protected $httpStatus;

    /**
     * Errors
     *
     * @var Error[]
     */
    protected $errors = [];

    /**
     * Base exception
     *
     * @param int         $httpStatus
     * @param Error[] $errors
     */
    public function __construct(
        $httpStatus = 400,
        array $errors = []
    ) {
        $this->setHttpStatus($httpStatus);
        $this->setErrors($errors);

        $message = "Glance Exception";

        parent::__construct($message, $httpStatus);
    }

    /**
     * Get HTTP Status Code
     *
     * @return int
     */
    public function getHttpStatus(): int
    {
        return $this->httpStatus;
    }

    /**
     * Set HTTP Status Code
     *
     * @param int $httpStatus
     *
     * @return void
     */
    public function setHttpStatus($httpStatus): void
    {
        $this->httpStatus = $httpStatus;
        $this->code = $httpStatus;
    }

    /**
     * Get errors
     *
     * @return Error[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Set errors
     *
     * @param Error[] $errors
     *
     * @return void
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    public function addError(Error $error): void
    {
        $this->errors[] = $error;
    }
}
