<?php

namespace Glance\ErrorMiddleware;

use JsonSerializable;

/**
 * Error Source
 *
 * According to JSON API:
 * "An object containing references to the source of the error, optionally
 * including any of the following members: pointer, parameter"
 *
 * @author Mario Simao <mario.simao@cern.ch>
 */
class ErrorSource implements JsonSerializable
{
    /** @var ?string */
    protected $pointer;

    /** @var ?string */
    protected $parameter;

    public function __construct(?string $pointer = null, ?string $parameter = null)
    {
        $this->pointer = $pointer;
        $this->parameter = $parameter;
    }

    public function jsonSerialize(): array
    {
        $array = [
            "pointer" => $this->pointer,
            "parameter" => $this->parameter,
        ];

        return array_filter($array);
    }

    /**
     * Pointer
     *
     * A JSON Pointer [RFC6901] to the associated entity in the request document
     *
     * E.g. "/data" for a primary data object, or "/data/attributes/title" for a
     * specific attribute.
     */
    public function getPointer(): ?string
    {
        return $this->pointer;
    }

    public function setPointer(?string $pointer): void
    {
        $this->pointer = $pointer;
    }

    /**
     * Parameter
     *
     * A string indicating which URI query parameter caused the error.
     */
    public function getParameter(): ?string
    {
        return $this->parameter;
    }

    public function setParameter(?string $parameter): void
    {
        $this->parameter = $parameter;
    }
}
