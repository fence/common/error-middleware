<?php

namespace Glance\ErrorMiddleware;

use Exception;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerInterface;
use Throwable;

/**
 * Error handler middleware
 *
 * @author Mario <mario.simao@cern.ch>
 *
 * @todo Add logging on unknown exceptions
 */
class ErrorMiddleware implements MiddlewareInterface
{
    /** @var ResponseFactoryInterface */
    private $responseFactory;

    /** @var LoggerInterface|null */
    private $logger;

    /** @var bool */
    private $debugMode;

    /** @var bool */
    private $useSentry;

    private function __construct(
        ResponseFactoryInterface $responseFactory,
        ?LoggerInterface $logger = null,
        bool $debugMode = false,
        bool $useSentry = false
    ) {
        $this->responseFactory = $responseFactory;
        $this->logger = $logger;
        $this->debugMode = $debugMode;
        $this->useSentry = $useSentry;
    }

    public static function create(): self
    {
        $responseFactory = Psr17FactoryDiscovery::findResponseFactory();

        return new self($responseFactory);
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    public function debugMode(bool $debugMode = true): void
    {
        $this->debugMode = $debugMode;
    }

    public function useSentry(bool $useSentry = true): void
    {
        $this->useSentry = $useSentry;
    }

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface {
        try {
            return $handler->handle($request);
        } catch (Throwable $exception) {
            return $this->handleException($exception);
        }
    }

    private function handleException(Throwable $exception): ResponseInterface
    {
        $httpStatus = 500;
        $errors = [];

        if ($exception instanceof BaseException) {
            $httpStatus = $exception->getHttpStatus();

            $errors = $exception->getErrors();
        } else {
            $error = [
                "status" => $httpStatus,
                "title"  => "Internal Error",
                "detail" => "An unexpected server error occurred.",
            ];

            if ($this->debugMode) {
                $error["type"]    = get_class($exception);
                $error["file"]    = $exception->getFile();
                $error["line"]    = $exception->getLine();
                $error["message"] = $exception->getMessage();
                $error["trace"]   = $exception->getTrace();
            }

            array_push($errors, $error);

            if ($this->useSentry) {
                $this->sendToSentry($exception);
            }
        }

        $this->logException($exception);

        $response = $this->responseFactory->createResponse($httpStatus);

        $body = json_encode([ "errors" => $errors ]);

        $response->getBody()->write($body);

        return $response->withAddedHeader("Content-Type", "application/json");
    }

    public function getResponseFactory(): ResponseFactoryInterface
    {
        return $this->responseFactory;
    }

    private function sendToSentry(Throwable $_exception): void
    {
        if (!function_exists("\\Sentry\\captureException")) {
            throw new Exception(
                "Sentry not installed. Install 'sentry/sdk' to send errors to Sentry."
            );
        }

        \Sentry\captureException($_exception);
    }

    private function logException(Throwable $exception): void
    {
        if (!$this->logger) {
            return;
        }

        $this->logger->error("+---------------+");
        $this->logger->error("|     FRAPI     |");
        $this->logger->error("| RUNTIME ERROR |");
        $this->logger->error("+---------------+");

        if ($exception instanceof BaseException) {
            foreach ($exception->getErrors() as $index => $error) {
                $humanIndex = (int) $index + 1;
                $this->logger->error("Error #$humanIndex");

                $status = $error->getStatus() ?? "";
                $this->logger->error("status --> $status");

                $code = $error->getCode() ?? "";
                $this->logger->error("code --> $code");

                $title = $error->getTitle() ?? "";
                $this->logger->error("title --> $title");

                $detail = $error->getDetail() ?? "";
                $this->logger->error("detail --> $detail");

                $this->logger->error("");
            }
        } else {
            $message = $exception->getMessage();
            $this->logger->error("Code: " . $exception->getCode());
            $this->logger->error("Message: " . $message);
        }

        $file = $exception->getFile();
        $line = $exception->getLine();
        $this->logger->error("File: {$file}($line)"); // Clickable on IDEs
        $this->logger->error("");

        $trace = explode("\n", $exception->getTraceAsString());
        foreach ($trace as $singleTrace) {
            $this->logger->error($singleTrace);
        }
    }
}
