<?php

namespace Glance\ErrorMiddleware;

use JsonSerializable;

/**
 * Error to be inserted on Api exceptions.
 *
 * Follows the rules from JSON API.
 * Non of the properties are mandatory.
 *
 * @author Mario Simao <mario.simao@cern.ch>
 * @link https://jsonapi.org/format/#error-objects
 */
class Error implements JsonSerializable
{
    /**
     * Error title
     *
     * A short, human-readable summary of the problem that SHOULD NOT change
     * from occurrence to occurrence of the problem, except for purposes of
     * localization.
     *
     * @var ?string
     */
    protected $title;

    /**
     * Error detail
     *
     * A human-readable explanation specific to this occurrence of the problem.
     * Like `$title`, this field’s value can be localized.
     *
     * @var ?string
     */
    protected $detail;

    /**
     * Error internal code
     *
     * An application-specific error code.
     *
     * @var ?int
     */
    protected $code;

    /**
     * Error HTTP status
     *
     * The HTTP status code applicable to this problem
     *
     * @var ?int
     */
    protected $status;

    /**
     * Error source
     *
     * @var ?ErrorSource
     */
    protected $source;

    public function __construct(
        ?string $title = null,
        ?string $detail = null,
        ?int $code = null,
        ?int $status = null,
        ?ErrorSource $source = null
    ) {
        $this->title = $title;
        $this->detail = $detail;
        $this->code = $code;
        $this->status = $status;
        $this->source = $source;
    }

    /**
     * Transform object to array to be JSON encoded.
     *
     * Null values are removed from the array.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $array = [
            "status" => $this->status,
            "code"   => $this->code,
            "source" => $this->source,
            "title"  => $this->title,
            "detail" => $this->detail
        ];

        return array_filter($array);
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(?string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getCode(): ?int
    {
        return $this->code;
    }

    public function setCode(?int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getSource(): ?ErrorSource
    {
        return $this->source;
    }

    public function setSource(?ErrorSource $source): self
    {
        $this->source = $source;

        return $this;
    }
}
