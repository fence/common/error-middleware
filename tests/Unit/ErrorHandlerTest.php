<?php

namespace Glance\ErrorMiddleware\Tests\Unit;

use Exception;
use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;
use Glance\ErrorMiddleware\ErrorMiddleware;
use Nyholm\Psr7\Factory\Psr17Factory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class ErrorMiddlewareTest extends TestCase
{
    public function testCreate(): void
    {
        $ErrorMiddleware = ErrorMiddleware::create();

        $this->assertInstanceOf(
            ResponseFactoryInterface::class,
            $ErrorMiddleware->getResponseFactory()
        );
    }

    public function testNoException(): void
    {
        $middleware = ErrorMiddleware::create();

        $factory = new Psr17Factory();
        $dummyResponse = $factory->createResponse();
        $dummyResponse->getBody()->write("Dummy response");
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willReturn($dummyResponse);

        $request = $factory->createServerRequest("GET", "https://cern.ch");
        $response = $middleware->process($request, $handler);

        $this->assertSame($dummyResponse, $response);
    }

    public function testGlanceException(): void
    {
        $middleware = ErrorMiddleware::create();

        $exception = new BaseException(
            400,
            [ new Error("Error title", "Error detail message.") ]
        );
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");
        $response = $middleware->process($request, $handler);

        $this->assertSame(400, $response->getStatusCode());
        $this->assertSame("application/json", $response->getHeaderLine("Content-Type"));

        $responseBody = json_encode([
            "errors" => [
                [
                    "title"  => "Error title",
                    "detail" => "Error detail message.",
                ],
            ],
        ]);
        $this->assertSame($responseBody, (string) $response->getBody());
    }

    public function testUnknownException(): void
    {
        $middleware = ErrorMiddleware::create();

        $exception = new Exception("Unknown exception.");
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");
        $response = $middleware->process($request, $handler);

        $this->assertSame(500, $response->getStatusCode());
        $this->assertSame("application/json", $response->getHeaderLine("Content-Type"));

        $responseBody = json_encode([
            "errors" => [
                [
                    "status" => 500,
                    "title"  => "Internal Error",
                    "detail" => "An unexpected server error occurred.",
                ],
            ],
        ]);
        $this->assertSame($responseBody, (string) $response->getBody());
    }

    public function testUnknownExceptionOnDebug(): void
    {
        $middleware = ErrorMiddleware::create();
        $middleware->debugMode();

        $exception = new Exception("Unknown exception.");
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");
        $response = $middleware->process($request, $handler);

        $this->assertSame(500, $response->getStatusCode());
        $this->assertSame("application/json", $response->getHeaderLine("Content-Type"));

        $responseBody = json_encode([
            "errors" => [
                [
                    "status"  => 500,
                    "title"   => "Internal Error",
                    "detail"  => "An unexpected server error occurred.",
                    "type"    => get_class($exception),
                    "file"    => $exception->getFile(),
                    "line"    => $exception->getLine(),
                    "message" => $exception->getMessage(),
                    "trace"   => $exception->getTrace(),
                ],
            ],
        ]);
        $this->assertSame($responseBody, (string) $response->getBody());
    }

    public function testGlanceExceptionWithLogger(): void
    {
        $logHandler = new \Monolog\Handler\TestHandler();
        $logger = new \Monolog\Logger("test");
        $logger->pushHandler($logHandler);

        $middleware = ErrorMiddleware::create();
        $middleware->setLogger($logger);

        $exception = new BaseException(
            400,
            [ new Error("Error title", "Error detail message.") ]
        );
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");
        $middleware->process($request, $handler);

        $this->assertTrue($logHandler->hasErrorRecords());
    }

    public function testUnknownExceptionWithLogger(): void
    {
        $logHandler = new \Monolog\Handler\TestHandler();
        $logger = new \Monolog\Logger("test");
        $logger->pushHandler($logHandler);

        $middleware = ErrorMiddleware::create();
        $middleware->setLogger($logger);

        $exception = new Exception("Unknown exception.");
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");
        $middleware->process($request, $handler);

        $this->assertTrue($logHandler->hasErrorRecords());
    }

    public function testUseSentryWithoutSentry(): void
    {
        $middleware = ErrorMiddleware::create();
        $middleware->useSentry();

        $exception = new Exception("Unknown exception.");
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");

        $this->expectExceptionMessage(
            "Sentry not installed. Install 'sentry/sdk' to send errors to Sentry."
        );
        $middleware->process($request, $handler);
    }

    /**
     * This should be the first test with Sentry.
     *
     * Imports Sentry mock.
     *
     * All tests after this one will behave as Sentry is installed.
     */
    public function testUseSentry(): void
    {
        require __DIR__ . "/sentry-mock.php";

        $middleware = ErrorMiddleware::create();
        $middleware->useSentry();

        $exception = new Exception("Unknown exception.");
        $handler = $this->createMock(RequestHandlerInterface::class);
        $handler->method("handle")->willThrowException($exception);

        $factory = new Psr17Factory();
        $request = $factory->createServerRequest("GET", "https://cern.ch");

        $response = $middleware->process($request, $handler);
        $this->assertSame(500, $response->getStatusCode());
    }
}
