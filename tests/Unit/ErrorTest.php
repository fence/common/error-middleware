<?php

namespace Glance\ErrorMiddleware\Tests\Unit;

use Glance\ErrorMiddleware\Error;
use Glance\ErrorMiddleware\ErrorSource;
use PHPUnit\Framework\TestCase;

final class ErrorTest extends TestCase
{
    public function testConstructor(): void
    {
        $source = new ErrorSource("/data/attributes/title");
        $error = new Error("Error title", "Error detail message.", 1234, 400, $source);

        $this->assertSame("Error title", $error->getTitle());
        $this->assertSame("Error detail message.", $error->getDetail());
        $this->assertSame(1234, $error->getCode());
        $this->assertSame(400, $error->getStatus());
        $this->assertSame($source, $error->getSource());
    }

    public function testJsonSerialize(): void
    {
        $source = new ErrorSource("/data/attributes/title");
        $error = new Error("Error title", "Error detail message.", 1234, 400, $source);

        $json = json_encode($error);

        $this->assertSame(
            [
                "status" => 400,
                "code"   => 1234,
                "source" => [ "pointer" => "/data/attributes/title" ],
                "title"  => "Error title",
                "detail" => "Error detail message."
            ],
            json_decode($json, true)
        );
    }

    public function testSetTitle(): void
    {
        $error = new Error();
        $error->setTitle("Error title");

        $this->assertSame("Error title", $error->getTitle());
    }

    public function testSetDetail(): void
    {
        $error = new Error();
        $error->setDetail("Error detail message.");

        $this->assertSame("Error detail message.", $error->getDetail());
    }

    public function testSetCode(): void
    {
        $error = new Error();
        $error->setCode(1234);

        $this->assertSame(1234, $error->getCode());
    }

    public function testSetStatus(): void
    {
        $error = new Error();
        $error->setStatus(400);

        $this->assertSame(400, $error->getStatus());
    }

    public function testSetSource(): void
    {
        $source = new ErrorSource("/data/attributes/title");
        $error = new Error();
        $error->setSource($source);

        $this->assertSame($source, $error->getSource());
    }
}
