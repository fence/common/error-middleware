<?php

namespace Glance\ErrorMiddleware\Tests\Unit;

use Glance\ErrorMiddleware\BaseException;
use Glance\ErrorMiddleware\Error;
use PHPUnit\Framework\TestCase;

final class BaseExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $httpStatus = 400;
        $error = new Error("Error title", "Error detail message.", 1234, $httpStatus);
        $errors = [ $error ];

        $exception = new BaseException($httpStatus, $errors);

        $this->assertSame($httpStatus, $exception->getCode());
        $this->assertSame($httpStatus, $exception->getHttpStatus());
        $this->assertSame($errors, $exception->getErrors());
        $this->assertSame("Glance Exception", $exception->getMessage());
    }

    public function testSetHttpStatus(): void
    {
        $exception = new BaseException(400);
        $exception->setHttpStatus(500);

        $this->assertSame(500, $exception->getHttpStatus());
        $this->assertSame(500, $exception->getCode());
    }

    public function testSetErrors(): void
    {
        $error = new Error("Error title", "Error detail message.", 1234, 400);

        $exception = new BaseException();
        $exception->setErrors([ $error ]);

        $this->assertSame([ $error ], $exception->getErrors());
    }

    public function testAddError(): void
    {
        $error = new Error("Error title", "Error detail message.", 1234, 400);

        $exception = new BaseException();
        $exception->addError($error);

        $this->assertSame([ $error ], $exception->getErrors());
    }
}
