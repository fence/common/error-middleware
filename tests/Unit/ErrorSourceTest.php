<?php

namespace Glance\ErrorMiddleware\Tests\Unit;

use Glance\ErrorMiddleware\ErrorSource;
use PHPUnit\Framework\TestCase;

final class ErrorSourceTest extends TestCase
{
    public function testConstructor(): void
    {
        $source = new ErrorSource("/data/attributes/title", "firstName");

        $this->assertSame("/data/attributes/title", $source->getPointer());
        $this->assertSame("firstName", $source->getParameter());
    }

    public function testSetPointer(): void
    {
        $source = new ErrorSource();
        $source->setPointer("/data/attributes/title");

        $this->assertSame("/data/attributes/title", $source->getPointer());
    }

    public function testSetParameter(): void
    {
        $source = new ErrorSource();
        $source->setParameter("firstName");

        $this->assertSame("firstName", $source->getParameter());
    }
}
